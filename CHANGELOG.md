## [4.2.2](https://gitlab.com/to-be-continuous/angular/compare/4.2.1...4.2.2) (2023-01-27)


### Bug Fixes

* Add registry name in all Docker images ([bbbcd88](https://gitlab.com/to-be-continuous/angular/commit/bbbcd88462922fac1a0daec443827141f26bc8aa))

## [4.2.1](https://gitlab.com/to-be-continuous/angular/compare/4.2.0...4.2.1) (2022-12-11)


### Bug Fixes

* build doesn't fail if not report found & fix reports path ([9dea336](https://gitlab.com/to-be-continuous/angular/commit/9dea3365d49e29a2f8094bbd0b1f5e1666177629))

# [4.2.0](https://gitlab.com/to-be-continuous/angular/compare/4.1.1...4.2.0) (2022-12-05)


### Features

* add a job generating software bill of materials ([7c96087](https://gitlab.com/to-be-continuous/angular/commit/7c960876dfa43522433be0fb3a31725a25ddbdb2))

## [4.1.1](https://gitlab.com/to-be-continuous/angular/compare/4.1.0...4.1.1) (2022-11-26)


### Bug Fixes

* support custom CA certificates with npm ([1613ca6](https://gitlab.com/to-be-continuous/angular/commit/1613ca6bda54a2d0d3e11057e2c4fe52bea925fd))

# [4.1.0](https://gitlab.com/to-be-continuous/angular/compare/4.0.0...4.1.0) (2022-11-23)


### Features

* add extra opts to install project deps ([b8a2cde](https://gitlab.com/to-be-continuous/angular/commit/b8a2cdee7931772e3028ad8ad1b3db2f10fedf8a))

# [4.0.0](https://gitlab.com/to-be-continuous/angular/compare/3.2.1...4.0.0) (2022-10-20)


### Features

* support Angular 14 ([a290513](https://gitlab.com/to-be-continuous/angular/commit/a290513955107c2d80ef242290984f1c52c58fec))


### BREAKING CHANGES

* in a multi module project, now the template build all projects by default (still customizable)

## [3.2.1](https://gitlab.com/to-be-continuous/angular/compare/3.2.0...3.2.1) (2022-10-14)


### Bug Fixes

* **lint:** support official SONAR_HOST_URL to trigger JSON report ([59567a8](https://gitlab.com/to-be-continuous/angular/commit/59567a8cc5f2b049057e5f2e9cd1f10e472df66b))

# [3.2.0](https://gitlab.com/to-be-continuous/angular/compare/3.1.0...3.2.0) (2022-10-10)


### Features

* **coverage:** add Cobertura report ([e268866](https://gitlab.com/to-be-continuous/angular/commit/e2688664b174d57b227819ddb6aeeae184c84832))

# [3.1.0](https://gitlab.com/to-be-continuous/angular/compare/3.0.1...3.1.0) (2022-10-04)


### Features

* normalize reports ([1f80fc9](https://gitlab.com/to-be-continuous/angular/commit/1f80fc9c31d512f004460e86eb57932ec2305a77))

## [3.0.1](https://gitlab.com/to-be-continuous/angular/compare/3.0.0...3.0.1) (2022-08-08)


### Bug Fixes

* **e2e:** skip if enabled var unset ([13eec55](https://gitlab.com/to-be-continuous/angular/commit/13eec5524043d7d4db7bd319f867660cc3ccea23))

# [3.0.0](https://gitlab.com/to-be-continuous/angular/compare/2.2.0...3.0.0) (2022-08-05)


### Features

* adaptive pipeline ([6bbe6cf](https://gitlab.com/to-be-continuous/angular/commit/6bbe6cfccd689f51149674bb63789c48ac2800c6))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [2.2.0](https://gitlab.com/to-be-continuous/angular/compare/2.1.0...2.2.0) (2022-05-01)


### Features

* configurable tracking image ([82f6bf1](https://gitlab.com/to-be-continuous/angular/commit/82f6bf10ab7185dfe6680935a44f53a6d572e96b))

# [2.1.0](https://gitlab.com/to-be-continuous/angular/compare/2.0.1...2.1.0) (2021-12-02)


### Features

* add support for npm scoped registries ([8522ee3](https://gitlab.com/to-be-continuous/angular/commit/8522ee3e3c2939c53284c07115d9804ec79bc9ab))

## [2.0.1](https://gitlab.com/to-be-continuous/angular/compare/2.0.0...2.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([1b35ffe](https://gitlab.com/to-be-continuous/angular/commit/1b35ffef89268f7c672f9dcea2e6b6b2ae8a9040))

## [2.0.0](https://gitlab.com/to-be-continuous/angular/compare/1.2.0...2.0.0) (2021-09-02)

### Features

* Change boolean variable behaviour ([6b573ea](https://gitlab.com/to-be-continuous/angular/commit/6b573ea47079cd20f7c7765409a8e76d1b5b1247))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.2.0](https://gitlab.com/to-be-continuous/angular/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([0541826](https://gitlab.com/to-be-continuous/angular/commit/054182647fa378014daeefd81c4f7fb2e433ce02))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/angular/compare/1.0.1...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([11ba7ae](https://gitlab.com/Orange-OpenSource/tbc/angular/commit/11ba7ae7b3eb5869f737a3a1b5bcb9220749bb39))

## [1.0.1](https://gitlab.com/Orange-OpenSource/tbc/angular/compare/1.0.0...1.0.1) (2021-05-12)

### Bug Fixes

* remove double $NG_WORKSPACE_DIR in JUnit report ([960fe3d](https://gitlab.com/Orange-OpenSource/tbc/angular/commit/960fe3dba7633c1a482adc5296a70465197c24db))

## 1.0.0 (2021-05-06)

### Features

* initial release ([6645031](https://gitlab.com/Orange-OpenSource/tbc/angular/commit/6645031f9a66dee712976a42f9b9e53e278fb8ad))
