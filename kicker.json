{
  "name": "Angular",
  "description": "Build, test and analyse your [Angular](https://angular.io/) projects",
  "template_path": "templates/gitlab-ci-angular.yml",
  "kind": "build",
  "variables": [
    {
      "name": "NG_CLI_IMAGE",
      "description": "The Docker image used to run Angular-CLI (`ng`) - **set the version required by your project**",
      "default": "registry.hub.docker.com/trion/ng-cli-karma:latest"
    },
    {
      "name": "NPM_CONFIG_REGISTRY",
      "description": "NPM [registry](https://docs.npmjs.com/configuring-your-registry-settings-as-an-npm-enterprise-user)",
      "type": "url",
      "advanced": true
    },
    {
      "name": "NPM_CONFIG_SCOPED_REGISTRIES",
      "description": "Space separated list of NPM [scoped registries](https://docs.npmjs.com/cli/v8/using-npm/scope#associating-a-scope-with-a-registry) (formatted as `@somescope:https://some.npm.registry/some/repo @anotherscope:https://another.npm.registry/another/repo`)",
      "advanced": true
    },
    {
      "name": "NG_WORKSPACE_DIR",
      "description": "Angular workspace directory",
      "default": ".",
      "advanced": true
    },
    {
      "name": "NG_INSTALL_EXTRA_OPTS",
      "description": "Extra options to install project dependencies (with [`npm ci`](https://docs.npmjs.com/cli/ci.html/))",
      "advanced": true
    },
    {
      "name": "NG_BUILD_ARGS",
      "description": "Angular [ng build](https://angular.io/cli/build) arguments",
      "default": "build",
      "advanced": true
    },
    {
      "name": "NG_TEST_ARGS",
      "description": "Angular [ng test](https://angular.io/cli/test) arguments",
      "default": "test --code-coverage --reporters progress,junit",
      "advanced": true
    }
  ],
  "features": [
    {
      "id": "lint",
      "name": "Angular lint",
      "description": "Angular lint analysis",
      "disable_with": "NG_LINT_DISABLED",
      "variables": [
        {
          "name": "NG_LINT_ARGS",
          "description": "Angular [ng lint](https://angular.io/cli/lint) arguments",
          "default": "lint",
          "advanced": true
        }
      ]
    },
    {
      "id": "publish",
      "name": "Publish",
      "description": "NPM [publish](https://docs.npmjs.com/cli/v6/commands/npm-publish) your angular project",
      "enable_with": "NG_PUBLISH_ENABLED",
      "variables": [
        {
          "name": "NG_PUBLISH_ARGS",
          "description": "npm [publish](https://docs.npmjs.com/cli/v6/commands/npm-publish) arguments",
          "default": "--verbose",
          "advanced": true
        },
        {
          "name": "NG_PUBLISH_PROJECTS",
          "description": "Space separated list of projects to publish. If no project is specified, all workspace projects are published.",
          "advanced": true
        },
        {
          "name": "NPM_PUBLISH_REGISTRY",
          "description": "NPM registry to publish to. If none is specified, uses GitLab project npm packages registry",
          "secret": true
        },
        {
          "name": "NPM_PUBLISH_TOKEN",
          "description": "NPM publication registry authentication token",
          "secret": true
        }
      ]
    },
    {
      "id": "e2e-test",
      "name": "e2e-test",
      "description": "Run your [e2e tests](https://angular.io/cli/e2e) on your angular project",
      "enable_with": "NG_E2E_ENABLED",
      "variables": [
        {
          "name": "NG_E2E_ARGS",
          "description": "ng [e2e](https://angular.io/cli/e2e) arguments",
          "default": "e2e",
          "advanced": true
        }
      ]
    },
    {
      "id": "sbom",
      "name": "Software Bill of Materials",
      "description": "This job generates a file listing all dependencies using [@cyclonedx/cyclonedx-npm](https://www.npmjs.com/package/@cyclonedx/cyclonedx-npm)",
      "disable_with": "NG_SBOM_DISABLED",
      "variables": [
        {
          "name": "NG_SBOM_VERSION",
          "description": "Version of the @cyclonedx/cyclonedx-npm used for SBOM analysis",
          "advanced": true
        },
        {
          "name": "NG_SBOM_OPTS",
          "description": "Options for @cyclonedx/cyclonedx-npm used for SBOM analysis",
          "default": "--omit dev",
          "advanced": true
        }
      ]
    }
  ]
}
